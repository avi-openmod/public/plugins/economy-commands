using System;
using System.Drawing;
using System.Threading.Tasks;

using Microsoft.Extensions.Localization;

using OpenMod.API.Commands;
using OpenMod.API.Users;
using OpenMod.Core.Commands;
using OpenMod.Core.Console;
using OpenMod.Extensions.Economy.Abstractions;

namespace Avi.OpenMod.Economy.Commands {

    /// <summary>
    /// Base balance economy command
    /// </summary>
    [Command("balance")]
    [CommandActor(typeof(IUser))]
    [CommandActor(typeof(ConsoleActor))]
    [CommandAlias("$")]
    [CommandAlias("money")]
    [CommandAlias("bal")]
    [CommandDescription("Gets caller's balance")]
    // ReSharper disable once ClassNeverInstantiated.Global
    public class BalanceCommand : Command {

        private readonly IStringLocalizer _stringLocalizer;
        private readonly IEconomyProvider _economyProvider;

        /// <inheritdoc />
        public BalanceCommand(
            IStringLocalizer stringLocalizer,
            IEconomyProvider economyProvider,
            IServiceProvider serviceProvider
        ) : base(serviceProvider) {
            _stringLocalizer = stringLocalizer;
            _economyProvider = economyProvider;
        }

        /// <inheritdoc />
        protected override async Task OnExecuteAsync() {

            ICommandActor caller = Context.Actor;

            if (caller is not IUser) {
                throw new UserFriendlyException(_stringLocalizer["fail:non_user_only_subcommands"]);
            }

            decimal balance = await _economyProvider.GetBalanceAsync(caller.Id, caller.Type);

            await caller.PrintMessageAsync(_stringLocalizer["balance:get", new {
                FormattedAmount = _stringLocalizer[
                    "formatted_amount",
                    new { Amount = balance, CurrencyName = _economyProvider.CurrencyName, CurrencySymbol = _economyProvider.CurrencySymbol }]
            }], Color.LightGreen);

        }

    }

}