using System;

using OpenMod.API.Plugins;
using OpenMod.Core.Plugins;

[assembly: PluginMetadata("Avi.OpenMod.Economy.Commands",
    Author = "aviadmini",
    DisplayName = "Avi Economy Commands",
    Website = "https://avi.pw/discord"
)]

namespace Avi.OpenMod.Economy.Commands {

    // ReSharper disable once UnusedType.Global
    internal class Plugin : OpenModUniversalPlugin {

        public Plugin(IServiceProvider serviceProvider) : base(serviceProvider) { }

    }

}