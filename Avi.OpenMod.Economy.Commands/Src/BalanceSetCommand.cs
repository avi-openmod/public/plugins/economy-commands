using System;
using System.Drawing;
using System.Threading.Tasks;

using Microsoft.Extensions.Localization;

using OpenMod.API.Commands;
using OpenMod.API.Users;
using OpenMod.Core.Commands;
using OpenMod.Core.Console;
using OpenMod.Core.Users;
using OpenMod.Extensions.Economy.Abstractions;

namespace Avi.OpenMod.Economy.Commands {

    [Command("set")]
    [CommandActor(typeof(IUser))]
    [CommandActor(typeof(ConsoleActor))]
    [CommandAlias(">")]
    [CommandDescription("Sets specified player's balance")]
    [CommandParent(typeof(BalanceCommand))]
    [CommandSyntax("<player> <amount>")]
    // ReSharper disable once UnusedType.Global
    internal class BalanceSetCommand : Command {

        private readonly IStringLocalizer _stringLocalizer;
        private readonly IUserManager _userManager;
        private readonly IEconomyProvider _economyProvider;

        public BalanceSetCommand(
            IStringLocalizer stringLocalizer,
            IUserManager userManager,
            IEconomyProvider economyProvider,
            IServiceProvider serviceProvider
        ) : base(serviceProvider) {
            _stringLocalizer = stringLocalizer;
            _userManager = userManager;
            _economyProvider = economyProvider;
        }

        protected override async Task OnExecuteAsync() {

            int amount = await Context.Parameters.GetAsync<int>(1);
            if (amount < 0) {
                throw new UserFriendlyException(_stringLocalizer["fail:invalid_amount"]);
            }

            string playerSearchQuery = await Context.Parameters.GetAsync<string>(0);

            IUser? user = await _userManager.FindUserAsync(KnownActorTypes.Player, playerSearchQuery, UserSearchMode.FindByNameOrId);
            if (user == null) {
                throw new UserFriendlyException(_stringLocalizer["fail:player_not_found"]);
            }

            ICommandActor caller = Context.Actor;
            await _economyProvider.SetBalanceAsync(user.Id, user.Type, amount);

            string fa = _stringLocalizer["formatted_amount", new {
                Amount = amount,
                CurrencyName = _economyProvider.CurrencyName, CurrencySymbol = _economyProvider.CurrencySymbol
            }];
            if (user.Id != caller.Id) { // tell caller it worked (unless caller = receiver)
                await caller.PrintMessageAsync(_stringLocalizer["balance:set_caller",
                    new { Actor = (ICommandActor) user, FormattedAmount = fa }], Color.LightGreen);
            }

            // tell receiver it worked
            await user.PrintMessageAsync(_stringLocalizer["balance:set_receiver",
                new { Actor = caller, FormattedAmount = fa }], Color.LightGreen);

        }

    }

}