using System.Linq;

using OpenMod.API.Commands;

namespace Avi.OpenMod.Economy.Commands {

    internal static class Extensions {

        public static string? GetRemainingString(this ICommandParameters parameters, int startIndex)
            => startIndex >= parameters.Length ? null : string.Join(" ", parameters.ToList().Skip(startIndex));

    }

}