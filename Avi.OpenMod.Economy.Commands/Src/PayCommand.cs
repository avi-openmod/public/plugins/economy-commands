using System;
using System.Drawing;
using System.Threading.Tasks;

using Microsoft.Extensions.Localization;

using OpenMod.API.Commands;
using OpenMod.API.Prioritization;
using OpenMod.API.Users;
using OpenMod.Core.Commands;
using OpenMod.Core.Users;
using OpenMod.Extensions.Economy.Abstractions;

namespace Avi.OpenMod.Economy.Commands {

    /// <summary>
    /// Base pay economy command
    /// </summary>
    [Command("pay")]
    [CommandActor(typeof(IUser))]
    [CommandDescription("Pays specified amount to the player")]
    [Priority(Priority = Priority.Low)]
    [CommandSyntax("<player> <amount> [reason]")]
    // ReSharper disable once UnusedType.Global
    public class PayCommand : Command {

        private readonly IStringLocalizer _stringLocalizer;
        private readonly IUserManager _userManager;
        private readonly IEconomyProvider _economyProvider;

        /// <inheritdoc />
        public PayCommand(
            IStringLocalizer stringLocalizer,
            IUserManager userManager,
            IEconomyProvider economyProvider,
            IServiceProvider serviceProvider
        ) : base(serviceProvider) {
            _stringLocalizer = stringLocalizer;
            _userManager = userManager;
            _economyProvider = economyProvider;
        }

        /// <inheritdoc />
        protected override async Task OnExecuteAsync() {

            string playerSearchQuery = await Context.Parameters.GetAsync<string>(0);
            decimal amount = await Context.Parameters.GetAsync<int>(1);

            if (amount < 0) {
                throw new UserFriendlyException(_stringLocalizer["fail:invalid_amount"]);
            }

            IUser? user = await _userManager.FindUserAsync(KnownActorTypes.Player, playerSearchQuery, UserSearchMode.FindByNameOrId);
            if (user == null) {
                throw new UserFriendlyException(_stringLocalizer["fail:player_not_found"]);
            }

            ICommandActor caller = Context.Actor;
            if (user.Id == caller.Id) {
                throw new UserFriendlyException(_stringLocalizer["fail:cannot_pay_self"]);
            }

            string? reason = Context.Parameters.GetRemainingString(2);
            try {
                await _economyProvider.UpdateBalanceAsync(caller.Id, caller.Type, -amount, reason);
                await _economyProvider.UpdateBalanceAsync(user.Id, user.Type, amount, reason);
            } catch (NotEnoughBalanceException e) {
                // doesn't matter if we lose some info from old exception, we still get meaningful exception for user
                string str = _stringLocalizer["fail:balance_too_low"];
                if (e.Balance == null) {
                    throw new NotEnoughBalanceException(str); // ReSharper disable once RedundantIfElseBlock
                } else {
                    throw new NotEnoughBalanceException(str, e.Balance.Value);
                }
            }

            string fa = _stringLocalizer["formatted_amount", new {
                Amount = amount,
                CurrencyName = _economyProvider.CurrencyName, CurrencySymbol = _economyProvider.CurrencySymbol
            }];
            await caller.PrintMessageAsync(_stringLocalizer["pay:payment_sent",
                new { Actor = user, FormattedAmount = fa }], Color.LightGreen);
            await user.PrintMessageAsync(_stringLocalizer["pay:payment_received",
                new { Actor = (IUser) caller, FormattedAmount = fa }], Color.LightGreen);

        }

    }

}