using System;
using System.Drawing;
using System.Threading.Tasks;

using Microsoft.Extensions.Localization;

using OpenMod.API.Commands;
using OpenMod.API.Users;
using OpenMod.Core.Commands;
using OpenMod.Core.Console;
using OpenMod.Core.Users;
using OpenMod.Extensions.Economy.Abstractions;

namespace Avi.OpenMod.Economy.Commands {

    [Command("of")]
    [CommandActor(typeof(IUser))]
    [CommandActor(typeof(ConsoleActor))]
    [CommandAlias("$")]
    [CommandDescription("Gets specified player's balance")]
    [CommandParent(typeof(BalanceCommand))]
    [CommandSyntax("<player>")]
    // ReSharper disable once UnusedType.Global
    internal class BalanceOfCommand : Command {

        private readonly IStringLocalizer _stringLocalizer;
        private readonly IUserManager _userManager;
        private readonly IEconomyProvider _economyProvider;

        public BalanceOfCommand(
            IStringLocalizer stringLocalizer,
            IUserManager userManager,
            IEconomyProvider economyProvider,
            IServiceProvider serviceProvider
        ) : base(serviceProvider) {
            _stringLocalizer = stringLocalizer;
            _userManager = userManager;
            _economyProvider = economyProvider;
        }

        protected override async Task OnExecuteAsync() {

            string playerSearchQuery = await Context.Parameters.GetAsync<string>(0);

            IUser? user = await _userManager.FindUserAsync(KnownActorTypes.Player, playerSearchQuery, UserSearchMode.FindByNameOrId);
            if (user == null) {
                throw new UserFriendlyException(_stringLocalizer["fail:player_not_found"]);
            }

            decimal balance = await _economyProvider.GetBalanceAsync(user.Id, user.Type);

            // tell caller the result
            await Context.Actor.PrintMessageAsync(_stringLocalizer["balance:check", new {
                Actor = user,
                FormattedAmount = _stringLocalizer[
                    "formatted_amount",
                    new { Amount = balance, CurrencyName = _economyProvider.CurrencyName, CurrencySymbol = _economyProvider.CurrencySymbol }]
            }], Color.Green);

        }

    }

}