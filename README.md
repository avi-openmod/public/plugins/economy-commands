Avi.OpenMod.Economy.Commands plugin
------------------------------------

Common commands for OpenMod economy providers:

- `/balance` (or `/$`, `/bal`, `/money`) -- shows caller's balance (command is only for players)
- `/balance of <player>` (or `/$ $ Nelson`) -- shows specified player's balance (by name or id)
- `/balance set <player> <amount>` (or `/bal > Nelson 100`)
- `/pay <player> <amount> [reason]` -- pays from specified amount to specified player with optional reason (command is only or players)
- `/apay <player> <amount> [reason]` -- adds specified amount to specified player's balance with optional reason

This package **only contains economy commands** and doesn't implement OpenMod economy provider. Economy provider implementations:
- [Avi.OpenMod.Unturned.XpEconomy](https://www.nuget.org/packages/Avi.OpenMod.Unturned.XpEconomy)
- [OpenMod.Economy](https://www.nuget.org/packages/OpenMod.Economy)

#### Installation:

Run `openmod install Avi.OpenMod.Economy.Commands` to install/update the plugin
To install beta versions use `om install Avi.OpenMod.Economy.Commands -Pre`.

License
=======

    Copyright 2021 aviadmini

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.